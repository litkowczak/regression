# --------------------------------------------------------------------------
# ------------  Metody Systemowe i Decyzyjne w Informatyce  ----------------
# --------------------------------------------------------------------------
#  Zadanie 4: Zadanie zaliczeniowe
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba, M Zieba
#  2019
# --------------------------------------------------------------------------

import pickle as pickle
FILE_PATH = 'model.pickle'
import numpy as np


def predict(x):
    """
    Funkcja pobiera macierz przykladow zapisanych w macierzy X o wymiarach NxD i zwraca wektor y o wymiarach Nx1,
    gdzie kazdy element jest z zakresu {0, ..., 9} i oznacza znak rozpoznany na danym przykladzie.
    :param x: macierz o wymiarach NxD
    :return: wektor o wymiarach Nx1
    """
    with open(FILE_PATH, 'rb') as handle:
        w = pickle.load(handle)
    
    preds=np.dot(x ,w.T).reshape(len(x),10)
    preds=np.argmax(preds,axis=1).reshape(len(x),1)
    return preds
    pass

