from content import *
import numpy as np
import pickle as pickle
import matplotlib.pyplot as plt
PICKLE_FILE_PATH = 'train.pkl'

def load_data():
    with open(PICKLE_FILE_PATH, 'rb') as f:
        return pickle.load(f)

images = load_data()[0] # images loaded shape(60000, 1296) - images in vector
labels = load_data()[1] # labels shape (60000,1)

print(np.shape(images))
print(np.shape(labels))

# #shufflowanie obrazkow i labelkow
# randomize=np.arange(len(labels))
# np.random.shuffle(randomize)
# X = images[randomize]
# Y = labels[randomize]

X_train, X_val = images[:45000], images[45000:]
Y_train, Y_val = labels[:45000], labels[45000:]

print(X_train.shape)
print(X_val.shape)
print(Y_train.shape)
print(Y_val.shape)

plt.imshow(np.reshape(X_train[0],(36,36)))
plt.title(Y_train[0].flatten())
plt.draw()
plt.waitforbuttonpress(0)
print(Y_train[:10].flatten())

def init_weights(x):
    (m,n)=x.shape
    k = len(np.unique(Y_train)) #10
    w = np.zeros((k,n,1))
    return w

def train_model(x,y,w, epochsnum, ETA, batch_size) :
    for i in range(w[0].shape):
        digit_class = i if i else 0
        w[i], = stochastic_gradient_descent(logistic_cost_function, x, ((y==digit_class)*1), w[i], eta=ETA
                                            ,epochs= epochsnum,mini_batch=batch_size)
